// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.18;

import "openzeppelin-contracts/contracts/access/Ownable.sol";

error ArrayLengthMismatch();

// This contract automate claiming rebase, increasing unlock time and voting for MIX pools
contract MixieThenaAutomation is Ownable {
    VoterV3 public constant voter =
        VoterV3(0x3A1D0952809F4948d15EBCe8d345962A282C4fCb); // https://bscscan.com/address/0x3A1D0952809F4948d15EBCe8d345962A282C4fCb
    RewardsDistributorV2 public constant rewardDistributor =
        RewardsDistributorV2(0xC6bE40f6a14D4C2F3AAdf9b02294b003e3967779); // https://bscscan.com/address/0xc6be40f6a14d4c2f3aadf9b02294b003e3967779
    VotingEscrow public constant votingEscrow =
        VotingEscrow(0xfBBF371C9B0B994EebFcC977CEf603F7f31c070D); // https://bscscan.com/address/0xfbbf371c9b0b994eebfcc977cef603f7f31c070d#code

    address[] public pools = [0x0f7d50A2728dE6f94B3aC8013031f527572cc9F1]; // MIX/WBNB https://bscscan.com/address/0x0f7d50a2728de6f94b3ac8013031f527572cc9f1
    uint256[] public weights = [100];

    uint public constant MAXTIME = 2 * 365 * 86400;

    function configure(
        address[] calldata _pools,
        uint256[] calldata _weights
    ) external onlyOwner {
        if (_pools.length != _weights.length) {
            revert ArrayLengthMismatch();
        }
        delete pools;
        for (uint idx = 0; idx < _pools.length; ++idx) {
            pools.push(_pools[idx]);
        }
        delete weights;
        for (uint idx = 0; idx < _weights.length; ++idx) {
            weights.push(_weights[idx]);
        }
    }

    function run(uint256 tokenId) external {
        // tokenId must have been approved for this contract
        rewardDistributor.claim(tokenId);
        try votingEscrow.increase_unlock_time(tokenId, MAXTIME) {} catch {}
        voter.vote(tokenId, pools, weights);
    }
}

interface VoterV3 {
    function vote(
        uint _tokenId,
        address[] calldata _poolVote,
        uint256[] calldata _weights
    ) external;
}

interface RewardsDistributorV2 {
    function claim(uint _tokenId) external returns (uint);
}

interface VotingEscrow {
    function increase_unlock_time(uint _tokenId, uint _lock_duration) external;
}
