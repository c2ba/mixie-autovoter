import { HardhatRuntimeEnvironment } from "hardhat/types";
import { DeployFunction } from "hardhat-deploy/types";
import { ethers } from "hardhat";

const deploy: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const network = await ethers.provider.getNetwork();
  if (network.chainId != 56n) {
    console.error(`Can only deploy on BNB chain with chain id 56`);
    process.exit(1);
  }

  const { deployments, getNamedAccounts } = hre;
  const { deploy } = deployments;

  const { deployer } = await getNamedAccounts();

  const deployResult = await deploy("MixieThenaAutomation", {
    contract: "MixieThenaAutomation",
    from: deployer,
    args: [],
    log: true,
  });
  if (deployResult.newlyDeployed) {
    await hre.run("verify:verify", {
      address: deployResult.address,
      constructorArguments: deployResult.args,
    });
  }
};
export default deploy;
