import { HardhatUserConfig } from "hardhat/config";
import "@nomicfoundation/hardhat-toolbox";
import "hardhat-preprocessor";
import "hardhat-deploy";
import fs from "fs";
import dotenv from "dotenv";

dotenv.config();

function getRemappings() {
  return fs
    .readFileSync("remappings.txt", "utf8")
    .split("\n")
    .filter(Boolean) // remove empty lines
    .map((line) => line.trim().split("="));
}

const config: HardhatUserConfig = {
  solidity: "0.8.18",
  preprocess: {
    eachLine: (hre) => ({
      transform: (line: string) => {
        if (line.match(/^\s*import /i)) {
          for (const [from, to] of getRemappings()) {
            if (line.includes(from)) {
              line = line.replace(from, to);
              break;
            }
          }
        }
        return line;
      },
    }),
  },
  paths: {
    sources: "./src",
    cache: "./cache_hardhat",
  },
  networks: {
    frame: {
      url: "http://127.0.0.1:1248",
      timeout: 300000,
    },
    deployBnbChain: {
      url: "http://127.0.0.1:1248",
      timeout: 300000,
    },
  },
  etherscan: {
    apiKey: {
      bsc: process.env?.BSCSCAN_API_KEY as string, // https://bscscan.com/
    },
  },
  namedAccounts: {
    deployer: 0,
  },
};

export default config;
